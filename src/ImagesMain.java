/**
 * File: ImagesMain
 * Author: Michelle John
 * Date: 18 November 2018
 * Purpose: Homework 2: Java2D BitMap Animation
 */
import javax.swing.*;
import java.awt.*;

/**
 * Main class and entry point into the program.
 */
public class ImagesMain {

  /**
   * Entry point to the program.
   *
   * @param args the arguments to use
   */
  public static void main(String[] args) {
    new ImagesMain();
  }

  /**
   * Constructor. Creates the frame and the {@link AnimationPanel}.
   */
  private ImagesMain() {
    JFrame frame = new JFrame("Java Animation");
    AnimationPanel animationPanel = new AnimationPanel();
    frame.setContentPane(animationPanel);
    frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    frame.pack();
    frame.setResizable(false);
    Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
    frame.setLocation((screen.width - frame.getWidth()) / 2, (screen.height - frame.getHeight()) / 2);

    frame.setVisible(true);
  }
}
