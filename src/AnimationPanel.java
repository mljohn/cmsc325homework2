/**
 * File: AnimationPanel
 * Author: Michelle John
 * Date: 18 November 2018
 * Purpose: Homework 2: Java2D BitMap Animation
 */
import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import static java.awt.RenderingHints.KEY_ANTIALIASING;
import static java.awt.RenderingHints.VALUE_ANTIALIAS_ON;

/**
 * The panel on which the animations are drawn. Extends {@link JPanel}.
 */
public class AnimationPanel extends JPanel {

  private int frameNumber;

  private int imageToUse = 0;

  private static int translateX = 0;
  private static int translateY = 0;
  private static double rotation = 0.0;
  private static double scaleX = 1.0;
  private static double scaleY = 1.0;

  private List<BufferedImage> imageList = new ArrayList<>();

  /**
   * Constructor. Builds the {@link JPanel} and sets the animation in motion.
   */
  AnimationPanel() {
    super();
    setPreferredSize(new Dimension(800, 600));

    Images images = new Images();
    imageList.add(images.getImage(Images.FACE_IMAGE, Color.PINK, Color.RED));
    imageList.add(images.getImage(Images.LETTER_M_IMAGE, Color.LIGHT_GRAY, Color.BLUE));
    imageList.add(images.getImage(Images.PANDA_IMAGE, Color.WHITE, Color.BLACK));
    imageList.add(images.getImage(Images.SQUARES_IMAGE, Color.RED, Color.GREEN));
    imageList.add(images.getImage(Images.STRIPES_IMAGE, Color.ORANGE, Color.YELLOW));

    Timer animationTimer;
    animationTimer = new Timer(500, ignored -> {
      if (frameNumber > 6) {
        imageToUse++;
        frameNumber = 0;
      } else {
        frameNumber++;
      }
      repaint();
      if (imageToUse >= imageList.size()) {
        imageToUse = 0;
      }
    });
    animationTimer.start();
  }

  @Override
  protected void paintComponent(Graphics graphics) {
    Graphics2D graphics2D = (Graphics2D) graphics;
    graphics2D.setRenderingHint(KEY_ANTIALIASING, VALUE_ANTIALIAS_ON);
    graphics2D.setPaint(Color.WHITE);
    graphics2D.fillRect(0, 0, getWidth(), getHeight());

    applyWindowToViewportTransformation(graphics2D, -50, 75, -35, 50, true);

    AffineTransform savedTransform = graphics2D.getTransform();
    switch (frameNumber) {
      case 1:
        translateX = 0;
        translateY = 0;
        scaleX = 1.0;
        scaleY = 1.0;
        rotation = 0;
        break;
      case 2:
        translateX = -10;
        translateY = 12;
        break;
      case 3:
        translateX = -10;
        translateY = 12;
        rotation = 55 * Math.PI / 180.0;
        break;
      case 4:
        translateX = -10;
        translateY = 12;
        rotation = -20 * Math.PI / 180.0;
        break;
      case 5:
        translateX = -10;
        translateY = 12;
        rotation = -20 * Math.PI / 180.0;
        scaleX = 3.0;
        scaleY = 1.5;
        break;
      case 6:
        translateX = 0;
        translateY = 0;
        scaleX = 1.0;
        scaleY = 1.0;
        rotation = 0;
      default:
        break;
    }

    graphics2D.translate(translateX, translateY);
    graphics2D.rotate(rotation);
    graphics2D.scale(scaleX, scaleY);
    graphics2D.drawImage(imageList.get(imageToUse), 0, 0, this);
    graphics2D.setTransform(savedTransform);
  }

  /**
   * Sets the viewport of the animation.
   *
   * @param graphics2D the {@link Graphics2D} object to manipulate
   * @param left the left coordinate of the viewport
   * @param right the right coordinate of the viewport
   * @param bottom the bottom coordinate of the viewport
   * @param top the top coordinate of the viewport
   * @param preserveAspect if the aspect is to be preserved
   */
  private void applyWindowToViewportTransformation(Graphics2D graphics2D, double left, double right, double bottom,
      double top, boolean preserveAspect) {
    int width = getWidth();
    int height = getHeight();
    if (preserveAspect) {
      double displayAspect = Math.abs((double) height/width);
      double requestedAspect = Math.abs((bottom - top) / (right - left));
      if (displayAspect > requestedAspect) {
        double excess = (bottom - top) * (displayAspect / requestedAspect - 1);
        bottom += excess / 2;
        top -= excess / 2;
      } else if (displayAspect < requestedAspect) {
        double excess = (right - left) * (requestedAspect / displayAspect - 1);
        right += excess / 2;
        left -= excess / 2;
      }
    }
    graphics2D.scale(width / (right - left), height / (bottom - top));
    graphics2D.translate(-left, -top);
  }
}
